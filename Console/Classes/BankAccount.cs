/*
In questa esercitazione il conto bancario supporta questo comportamento:
?? Esiste un numero di 10 cifre che identifica in modo univoco il conto.
ok Esiste una stringa in cui vengono archiviati i nomi dei titolari.
È possibile recuperare il saldo.
Il conto accetta versamenti.
Il conto accetta prelievi.
Il saldo iniziale deve essere positivo.
I prelievi non possono risultare in un saldo negativo.
*/
using System;

namespace Classes
{
    public class BankAccount
    {
        public string Number { get; }
        public string Owner { get; set; }
        public decimal Balance { get; }

        public BankAccount(string name, decimal initialBalance)
        {
            this.Owner = name;
            this.Balance = initialBalance;
        }

        public void MakeDeposit(decimal amount, DateTime date, string note)
        {
        }

        public void MakeWithdrawal(decimal amount, DateTime date, string note)
        {
        }
    }
} 