﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
        try 
        {
            string s = "null";
            Console.WriteLine(s.Length);
        /*
        catch (ExceptionType [e]) when (expr)
        comportamento del when
        if(expr)
            eseguo blocco catch
        else
            lancio un eccezione
        */
        } catch (Exception e) when (LogException(e))
        {
            Console.WriteLine("catch");
        }
        Console.WriteLine("Exception must have been handled");

        Console.WriteLine(nameof(System.String));
        int j = 5;
        Console.WriteLine(nameof(j));
        List<string> names = new List<string>();
        Console.WriteLine(nameof(names));

        var messages = new Dictionary<int, string>
        {
            [404] = "Page not Found",
            [302] = "Page moved, but left a forwarding address.",
            [500] = "The web server can't come out to play today."
        };

        Console.WriteLine(messages[302]);
    }

    private static bool LogException(Exception e)
    {
        Console.WriteLine($"\tIn the log routine. Caught {e.GetType()}");
        Console.WriteLine($"\tMessage: {e.Message}");
        return false;
        //return true;
    }
}