﻿using static System.Console;

public class A
{
   private int value = 10;

   public class B : A
   {
       public int GetValue()
       {
           return this.value;
       }
   }
}

public class C : A
{
//    public int GetValue()
//    {
//        return this.value;
//    }
}


public class A2
{
    public virtual void Method1()
    {
        WriteLine("A.Method1()");
    }
}

public class B : A2
{ 
    //Non è possibile eseguire l'override di un metodo se il metodo della classe base non è contrasegnato con "virtual"
     public override void Method1()
     {
        WriteLine("B.Method1()");
     }
}

class Program
{
    static void Main(string[] args)
    {
        var b = new A.B();
        WriteLine(b.GetValue());

        var bTopLevel = new B();
        bTopLevel.Method1();
    }
}
