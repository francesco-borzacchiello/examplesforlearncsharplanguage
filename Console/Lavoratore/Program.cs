﻿using static System.Console;
using Autonomo;
using Dipendente;

namespace Lavoratore
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine(new Worker("Marco", "Rossi", 800.0));
            Independent i = new Independent();
            WriteLine(i.f());
            WriteLine(new Employee().h());
        }
    }
}
