namespace Lavoratore
{
    public class Worker
    {
        public Worker(string name, string surname, double salary) 
        {
            this.name = name;
            this.surname = surname;
            this.salary = salary;  
        }
        
        public string name{get;}
        public string surname {get;}
        public double salary{get; private set;}

        public override string ToString()
        {
            return $"{name} {surname} {salary}";
        }
    }
}