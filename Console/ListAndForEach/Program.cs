﻿using System;
using System.Collections.Generic;

namespace ListAndForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<string> { "Francesco", "Ana", "Felipe"};
            
            PrintList<String>(names);
            
            OrizzontalLine();

            names.Add("Maria");
            names.Add("Bill");
            names.Remove("Ana");
            PrintList<String>(names);

            OrizzontalLine();

            PrintList<int>(new List<int>{10,20,30,40,50,650});

            OrizzontalLine();
            
            Console.WriteLine($"My name is {names[0]}.");
            Console.WriteLine($"I've added {names[2]} and {names[3]} to the list.");

            Console.WriteLine($"The list has {names.Count} people in it");

            OrizzontalLine();
            
            //Ricerca
            var index = names.IndexOf("Felipe");
            if (index != -1)
                Console.WriteLine($"The name {names[index]} is at index {index}");

            var notFound = names.IndexOf("Not Found");
            Console.WriteLine($"When an item is not found, IndexOf returns {notFound}");

            OrizzontalLine();

            //Ordinamento
            names.Sort();
            foreach (var name in names)
            {
                Console.WriteLine($"Hello {name.ToUpper()}!");
            }

            OrizzontalLine();

            var fibonacciNumbers = new List<int> {1, 1};

            var previous = fibonacciNumbers[fibonacciNumbers.Count - 1];
            var previous2 = fibonacciNumbers[fibonacciNumbers.Count - 2];

            fibonacciNumbers.Add(previous + previous2);

            Console.WriteLine("Sequenza di fibonacci:");
            /* 
            foreach(var item in fibonacciNumbers)
                Console.WriteLine(item);
             */
            
            PrintList<int>(GeneratesFibonacciSequence(20));

        }

        public static void OrizzontalLine() => Console.WriteLine(new string('_', 40));

        public static void PrintList<T>(List<T> list)
        {
            foreach (var name in list)
            {
                Console.WriteLine($"Hello {name}!");
            }
        }

        public static List<int> GeneratesFibonacciSequence(int limit)
        {
            // Provare a scrivere il codice per generare i primi 20 numeri nella successione. Tenere presente che il 20° numero di Fibonacci è 6765
            if(limit < 0)
                return null;
            else if(limit == 1)
                return new List<int>{1};
            else if(limit == 2)
                return new List<int>{1, 1};
            
            var fibonacci = new List<int>{1,1};

            for(int i = 3; i <= limit; i++)
               fibonacci.Add(fibonacci[fibonacci.Count - 1] + fibonacci[fibonacci.Count - 2]);

            return fibonacci;
        }
    }
}
