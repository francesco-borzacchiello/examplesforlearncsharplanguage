﻿using System;
using Classes;

namespace MainConstructs
{
    class Program
    {
        static void Main(string[] args)
        {
            TestIf();
            TestWhile();
            TestDoWhile();
            TestFor();
            Console.WriteLine("sum of divisors of 3 from 0 to 20 : " + Exercise());
            Console.WriteLine("sum of divisors of 5 from 53 to 200 : " + ExercisePersonalized(53, 200, 5));
        }

        public static void OrizzontalLine() => Console.WriteLine(new string('_', 40));

        private static void TestIf()
        {
            int a = 5;
            int b = 6;
            if (a + b > 10)
                Console.WriteLine("The answer is greater than 10.");

            b = 3;
            if (a + b > 10)
                Console.WriteLine("The answer is greater than 10.");
            else
                Console.WriteLine("The answer is not greater than 10");
            
            if ((a + b > 10) || true)
            {
                Console.WriteLine("The answer is greater than 10");
            }
            else
            {
                Console.WriteLine("The answer is not greater than 10");
            }

            OrizzontalLine();

            a = 5;
            b = 3;
            int c = 4;
            if ((a + b + c > 10) && (a == b))
            {
                Console.WriteLine("The answer is greater than 10");
                Console.WriteLine("And the first number is equal to the second");
            }
            else
            {
                Console.WriteLine("The answer is not greater than 10");
                Console.WriteLine("Or the first number is not equal to the second");
            }

            OrizzontalLine();

            if ((a + b + c > 10) || (a == b))
            {
                Console.WriteLine("The answer is greater than 10");
                Console.WriteLine("Or the first number is equal to the second");
            }
            else
            {
                Console.WriteLine("The answer is not greater than 10");
                Console.WriteLine("And the first number is not equal to the second");
            }

            OrizzontalLine();
        }
        private static void TestWhile()
        {
            int counter = 0;
            while (counter < 10)
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
                counter++;
            }

            OrizzontalLine();

            counter = 0;
            while (counter < 10)
                Console.WriteLine($"Hello World! The counter is {counter++}");

            OrizzontalLine();

            counter = 0;
            while (counter < 10)
                Console.WriteLine($"Hello World! The counter is {++counter}");

            OrizzontalLine();

        }
        private static void TestDoWhile()
        {
            Console.WriteLine("Do-While:");
            int counter = 0;
            do
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
                counter++;
            } while (counter < 10);

            OrizzontalLine();
        }
        private static void TestFor()
        {
            Console.WriteLine("For:");
            for(int counter = 0; counter < 10; counter++)
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
            }

            OrizzontalLine();

            Console.WriteLine("For, print reverse:");
            for(int counter = 9; counter >= 0; counter--)
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
            }

            OrizzontalLine();

            MatrixExample();
        }
        private static void MatrixExample()
        {
            for (int row = 1; row < 11; row++)
            {
                Console.WriteLine($"The row is {row}");
            }
            
            OrizzontalLine();

            for (char column = 'a'; column < 'k'; column++)
            {
                Console.WriteLine($"The column is {column}");
            }

            OrizzontalLine();

            for (int row = 1; row < 11; row++)
            {
                for (char column = 'a'; column < 'k'; column++)
                {
                    Console.WriteLine($"The cell is ({row}, {column})");
                }
            }
                        
            OrizzontalLine();

            for (int row = 1; row < 11; row++)
            {
                for (char column = 'a'; column < 'k'; column++)
                {
                    Console.Write($"({row}, {column})\t");
                }
                Console.WriteLine();
            }
        }
        private static int Exercise()
        {
            //Provare a scrivere codice C# per ottenere la somma di tutti i numeri interi da 1 a 20 divisibili per 3
            OrizzontalLine();
            int sum = 0;
            for(int i = 0; i <=20; i+=3)
                sum+=i;
            
            return sum;
        }

        private static int ExercisePersonalized(int min, int max, int dividersOf)
        {
            //Provare a scrivere codice C# per ottenere la somma di tutti i numeri interi da min a max divisibili per dividersOf
            OrizzontalLine();
            int sum = 0;
            for(int i = min; i <=max; i++)
                if(i%dividersOf == 0)
                    sum+=i;
            return sum;
        }
   }
}
