﻿using static System.Console;
using System.Linq;

public class Person
{
    /*Attributi di sola lettura, 
    rendendo il set privato non è possbile cambiare il loro valore all'esterno della classe*/
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    //Di sola lettura, non modificabile nemmeno nella classe stessa
    public string MiddleName { get; } = "";

    public Person(string first, string middle, string last)
    {
        FirstName = first;
        MiddleName = middle;
        LastName = last;
    }

    public Person(string first, string last)
    {
        FirstName = first;
        LastName = last;
    }

    public override string ToString() => $"{FirstName} {LastName}";
    
    public string AllCaps() => ToString().ToUpper();
}

public class Program
{
    public static void Main()
    {
        var p = new Person("Bill", "Wagner");
        WriteLine($"The name, in all caps: {p.AllCaps()}");
        WriteLine($"The name is: {p}");
        WriteLine(p.FirstName + " " + p.LastName + " \"" + p.MiddleName + "\"");

        var phrase = "the quick brown fox jumps over the lazy dog";
        var wordLength = from word in phrase.Split(' ') select word.Length;
        //WriteLine($"wordLength: {wordLength}");
        //WriteLine($"phrase.Split(' '): {phrase.Split(' ')}");
        var average = wordLength.Average();
        WriteLine($"The average word length is: {wordLength.Average():F2}");

        string s = null;
        //WriteLine(s.Length);      NullReferenceException
        WriteLine($"s.Lenght: {s?.Length}");

        char? c = s?[0];
        WriteLine($"c.HasValue: {c.HasValue} c: \"{c}\"");

        //public char[] ToCharArray (int startIndex, int length);
        //public System.Collections.IEnumerator GetEnumerator ();
        //public bool MoveNext ();
        bool? hasMore = s?.ToCharArray()?.GetEnumerator()?.MoveNext();
        WriteLine(hasMore.HasValue && hasMore.Value);

        hasMore = s?.ToCharArray()?.GetEnumerator()?.MoveNext() ?? false;
        WriteLine(hasMore);
    }
}