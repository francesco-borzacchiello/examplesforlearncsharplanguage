﻿using System;

namespace OperationsBetweenVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 18;
            int b = 6;
            int c = a + b;
            Console.WriteLine("a = " + a + "\nb = " + b);
            Console.WriteLine("a + b = " + c);
            Console.WriteLine("a + b = " + (a + b));

            c = a - b;
            Console.WriteLine("a - b = " + c);
            c = a * b;
            Console.WriteLine("a * b = " + c);
            c = a / b;
            Console.WriteLine("a / b = " + c);
            try
            {
                c = a / 0;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("operation invalid!!");
            }

            OrizzontalLine();

            //Precedenza tra operatori come in matematica
            a = 5;
            b = 4;
            c = 2;
            int d = a + b * c;
            Console.WriteLine("a = " + a + "\nb = " + b + "\nc = " + c);
            Console.WriteLine("a + b * c = " + d);

            d = (a + b) * c;
            Console.WriteLine("(a + b) * c = " + d);
            d = (a + b) - 6 * c + (12 * 4) / 3 + 12;
            //   9 - 6 * c + 48 / 3 + 12
            //   9 - 12 + 16 + 12 = 25
            Console.WriteLine("(a + b) - 6 * c + (12 * 4) / 3 + 12 = " + d);

            OrizzontalLine();
 
            a = 7;
            b = 4;
            c = 3;
            //La divisione tronca
            d = (a + b) / c;
            Console.WriteLine("a = " + a + "\nb = " + b + "\nc = " + c);
            Console.WriteLine($"(a + b) / c = {d}");
            //Operatore modulo
            int e = (a + b) % c;
            Console.WriteLine($"remainder: {e}");

            OrizzontalLine();

            int max = int.MaxValue;
            int min = int.MinValue;
            Console.WriteLine($"The range of integers is {min} to {max}");

            //Overflow
            int what = max + 3;
            Console.WriteLine($"An example of overflow: {what}");

            OrizzontalLine();
            TestDouble();
        }

        public static void OrizzontalLine() => Console.WriteLine(new string('_', 40));

        private static void TestDouble()
        {
            double a = 5;
            double b = 4;
            double c = 2;
            double d = (a + b) / c;
            Console.WriteLine("Double:");
            Console.WriteLine("a = " + a + "\nb = " + b + "\nc = " + c);
            Console.WriteLine($"(a + b) / c = {d}");

            OrizzontalLine();

            a = 19;
            b = 23;
            c = 8;
            d = (a + b) / c;
            Console.WriteLine("a = " + a + "\nb = " + b + "\nc = " + c);
            Console.WriteLine($"(a + b) / c = {d}");

            OrizzontalLine();

            double max = double.MaxValue;
            double min = double.MinValue;
            Console.WriteLine($"The range of double is {min} to {max}");

            //Arrotondamento
            double third = 1.0 / 3.0;
            Console.WriteLine($"1.0 / 3.0 = {third}");

            OrizzontalLine();
            TestDecimal();
        }

        private static void TestDecimal()
        {
            decimal min = decimal.MinValue;
            decimal max = decimal.MaxValue;
            Console.WriteLine($"The range of the decimal type is {min} to {max}");

            double a = 1.0;
            double b = 3.0;
            Console.WriteLine("Double:");
            Console.WriteLine($"1.0 / 3.0 = {a / b}");

            decimal c = 1.0M;
            decimal d = 3.0M;
            Console.WriteLine("Decimal:");
            Console.WriteLine($"1.0 / 3.0 = {c / d}");

            OrizzontalLine();
            Console.WriteLine($"Area of a circle with a radius of 2,5cm = {CircleArea(2.5)}");
        }

        private static double CircleArea(double raggio)
        {
            return Math.Pow(raggio, 2) * Math.PI;
        }
    }
}
