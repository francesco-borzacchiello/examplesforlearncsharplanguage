﻿using System;

namespace String
{
class Program
    {
        /*static void Main(string[] args)
        {
            string aFriend = "Francesco";
            Console.WriteLine(aFriend);

            aFriend = "Franco";
            Console.WriteLine(aFriend);

            Program.OrizzontalLine();

            Console.WriteLine("Hello " + aFriend);

            //Interpolazione di stringhe
            Console.WriteLine($"Hello {aFriend}");

            Program.OrizzontalLine();

            string firstFriend = "Maria";
            string secondFriend = "Sage";
            Console.WriteLine($"My friends are {firstFriend} and {secondFriend}");

            //Lunghezza stringhe
            Console.WriteLine($"The name {firstFriend} has {LengthString(firstFriend)} letters.");
            Console.WriteLine($"The name {secondFriend} has {LengthString(secondFriend)} letters.");

            Program.OrizzontalLine();

            //La modifica non viene eseguita in place, ma viene creata una nuova stringa
            string greeting = "      Hello World!       ";
            Console.WriteLine($"[{greeting}]");

            string trimmedGreeting = greeting.TrimStart();
            Console.WriteLine($"[{trimmedGreeting}]");

            trimmedGreeting = greeting.TrimEnd();
            Console.WriteLine($"[{trimmedGreeting}]");

            trimmedGreeting = greeting.Trim();
            Console.WriteLine($"[{trimmedGreeting}]");

            Program.OrizzontalLine();

            //Sostituzione
            string sayHello = "Hello World!";
            Console.WriteLine(sayHello);
            sayHello = sayHello.Replace("Hello", "Greetings");
            Console.WriteLine(sayHello);

            Program.OrizzontalLine();

            Console.WriteLine(sayHello.ToUpper());
            Console.WriteLine(sayHello.ToLower());

            Program.OrizzontalLine();

            //Ricerca
            string songLyrics = "You say goodbye, and I say hello";
            Console.WriteLine("\"" + songLyrics +"\" contains \"goodbye\" :" + songLyrics.Contains("goodbye"));
            Console.WriteLine("\"" + songLyrics +"\" contains \"greetings\" :"+ songLyrics.Contains("greetings"));

            //Cercare "You" o "goodbye" all'inizio di una stringa
            Console.Write("\"" + songLyrics +"\" start with \"You\" or \"goodbye\" : ");
            Console.WriteLine(songLyrics.StartsWith("You") || songLyrics.StartsWith("goodbye"));
            
            //Cercare "hello" o "goodbye" alla fine di una stringa.
            Console.Write("\"" + songLyrics +"\" end with \"hello\" or \"goodbye\" : ");
            Console.WriteLine(songLyrics.EndsWith("hello") || songLyrics.EndsWith("goodbye"));

        
        }

        public static void OrizzontalLine() => Console.WriteLine(new string('_', 40));

		public static int LengthString(string stringa) => stringa.Length;*/
    }
}
