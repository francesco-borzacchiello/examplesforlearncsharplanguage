﻿using System;

namespace ExamplesForLearnCSharpLanguage
{	
	public class Utility
	{
		public static void OrizzontalLine() => Console.WriteLine(new string('_', 40));

		public static int LengthString(string stringa) => stringa.Length;
	}
}
