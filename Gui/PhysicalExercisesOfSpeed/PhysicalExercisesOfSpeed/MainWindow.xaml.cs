﻿using PhysicalExercisesOfSpeed.Moduli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicalExercisesOfSpeed
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, RoutedEventArgs e)
        {
            MainContainer.Children.Add(new Home(this));
        }

        public void ChangeModuleVisible(String variableSelected)
        {
            MainContainer.Children.Clear();
            switch(variableSelected)
            {
                case "Velocità":
                    MainContainer.Children.Add(new CalculateSpeed());
                    break;
                case "Delta S":
                    MessageBox.Show("2");
                    break;
                case "Delta t":
                    MessageBox.Show("3");
                    break;
                case "S1":
                    MessageBox.Show("4");
                    break;
                case "S2":
                    MessageBox.Show("5");
                    break;
                case "t1":
                    MessageBox.Show("6");
                    break;
                case "t2":
                    MessageBox.Show("7");
                    break;
                default:
                    MessageBox.Show($"ChangeModuleVisible({variableSelected})");
                    break;
            }
        }
    }
}
