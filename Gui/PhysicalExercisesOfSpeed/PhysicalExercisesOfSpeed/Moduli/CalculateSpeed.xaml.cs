﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicalExercisesOfSpeed.Moduli
{
    /// <summary>
    /// Logica di interazione per CalculateSpeed.xaml
    /// </summary>
    public partial class CalculateSpeed : UserControl
    {
        public CalculateSpeed()
        {
            InitializeComponent();
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double result = (double.Parse(S2.Text) - double.Parse(S1.Text)) /
                        (double.Parse(T2.Text) - double.Parse(T1.Text));
                MessageBox.Show(result.ToString());
                SpeedResult.Content = result.ToString();
            }
            catch (Exception) { }
        }
    }
}
