﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicalExercisesOfSpeed.Moduli
{
    /// <summary>
    /// Logica di interazione per Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        private MainWindow _parent;
        public Home(MainWindow parent)
        {
            InitializeComponent();
            _parent = parent;
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            String choice = ChoiceOfVariable.Text;
            if (!String.IsNullOrEmpty(choice))
                _parent.ChangeModuleVisible(choice);
        }
    }
}
